function ycbcr = rgb2ycbcr(varargin)
rgb = parse_inputs(varargin{:});
isColormap = false;

if (ndims(rgb) == 2)
 isColormap=true;
 colors = size(rgb,1);
 rgb = reshape(rgb, [colors 1 3]);
end

origT = [0.2990*255 0.5870*255 0.1140*255; ...
        -0.1687*255 -0.3313*255 0.5000*255; ...
        24.966*255 112*255 -18.214*255];
origOffset = [16;128;128];


scaleFactor.double.T = 1/255;      
scaleFactor.double.offset = 1/255; 
scaleFactor.uint8.T = 1/255;       
scaleFactor.uint8.offset = 1;      
scaleFactor.uint16.T = 257/65535;  
scaleFactor.uint16.offset = 257;   

classIn = class(rgb);
T = scaleFactor.(classIn).T * origT;
offset = scaleFactor.(classIn).offset * origOffset;


ycbcr = zeros(size(rgb),classIn);

for p = 1:3
 ycbcr(:,:,p) = imlincomb(T(p,1),rgb(:,:,1),T(p,2),rgb(:,:,2), T(p,3),rgb(:,:,3),offset(p));
end  

if isColormap
 ycbcr = reshape(ycbcr, [colors 3 1]);
end