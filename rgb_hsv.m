function f=rgb_hsv(a)
%%% fungsi ini digunakan untuk merubah citra I pada ruang warna RGB menjadi
%%% ruang warna HSV/HSI

r=double(a(:,:,1));
g=double(a(:,:,2));
b=double(a(:,:,3));
[m,n]=size(r);

for i=1:m
    for j=1:n
        vektor=[r(i,j),g(i,j),b(i,j)];
        maks=max(vektor);
        minim = min(vektor);
        delta = maks-minim;
        
        if(r(i,j)==maks)&(delta~=0)
            h(i,j)=60*((g(i,j)-b(i,j))/delta);
        elseif(g(i,j)==maks)&(delta~=0)
            h(i,j)=60*((b(i,j)-r(i,j))/delta+2);
        elseif(b(i,j)==maks)&(delta~=0)
            h(i,j)=60*((g(i,j)-r(i,j))/delta+4);
        elseif(maks==0) 
            h(i,j)=0;
        end
        if(maks~=0)
            s(i,j)=delta/maks;
        else
            s(i,j)=0;
        end
        
        v(i,j)=maks;   
    end
end
h=h/255;
v=v/255;
f=cat(3,h,s,v);
