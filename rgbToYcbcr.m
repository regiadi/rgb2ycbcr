rgbImage = imread('image.png');  %# A sample RGB image
A = [65.481 -37.797 112; ...       %# A 3-by-3 matrix of scale factors
     128.553 -74.203 -93.786; ...
     24.966 112 -18.214];

%# First convert the RGB image to double precision, scale its values to the
%#   range 0 to 1, reshape it to an N-by-3 matrix, and multiply by A:
ycbcrImage = reshape(double(rgbImage)./255,[],3)*A;

%# Shift each color plane (stored in each column of the N-by-3 matrix):
ycbcrImage(:,1) = ycbcrImage(:,1)+16;
ycbcrImage(:,2) = ycbcrImage(:,2)+128;
ycbcrImage(:,3) = ycbcrImage(:,3)+128;

%# Convert back to type uint8 and reshape to its original size:
ycbcrImage = reshape(uint8(ycbcrImage),size(rgbImage));
figure, imshow(ycbcrImage);
figure, imshow(ycbcrImage(:, :, 1));
figure, imshow(ycbcrImage(:, :, 2));
figure, imshow(ycbcrImage(:, :, 3));